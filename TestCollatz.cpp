// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(500, 1000)), make_tuple(500, 1000, 179));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(111, 60000)), make_tuple(111, 60000, 340));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(555, 1258)), make_tuple(555, 1258, 182));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(888, 9999)), make_tuple(888, 9999, 262));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(780, 1000000)), make_tuple(780, 1000000, 525));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(2,1000000)), make_tuple(2, 1000000, 525));
}



// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}


//-----
// cycle_length
// -----

TEST(CollatzFixture, cycle_length0) {
    ASSERT_EQ(cycle_length (9), 20);
}

TEST(CollatzFixture, cycle_length1) {
    ASSERT_EQ(cycle_length (1000), 112);
}

TEST(CollatzFixture, cycle_length2) {
    ASSERT_EQ(cycle_length (1000000), 153);
}

TEST(CollatzFixture, cycle_length3) {
    ASSERT_EQ(cycle_length (888), 73);
}

TEST(CollatzFixture, cycle_length4) {
    ASSERT_EQ(cycle_length (7777), 84);
}

TEST(CollatzFixture, cycle_length5) {
    ASSERT_EQ(cycle_length (98765), 54);
}

//-----
// solve
// -----

TEST(CollatzFixture,  max_length0 ) {
    ASSERT_EQ(max_length (50, 2000), 182);
}

TEST(CollatzFixture,  max_length1 ) {
    ASSERT_EQ( max_length (2, 10000), 262);
}

TEST(CollatzFixture,  max_length2 ) {
    ASSERT_EQ( max_length (7777, 8888), 252);
}

TEST(CollatzFixture,  max_length3 ) {
    ASSERT_EQ( max_length (67, 20000), 279);
}

TEST(CollatzFixture,  max_length4 ) {
    ASSERT_EQ( max_length (96, 12345), 268);
}

